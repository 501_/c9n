import re, math

"""
English Term	Variable Name
Труба	tube
Трубопроводная система	pipelineSystem
Наружный диаметр (трубы)	outerDiameter
Теплоноситель	heatCarrier
Масса сухой трубы	dryTubeMass
Масса заполненной трубы	filledTubeMass
Толщина стенки	wallThickness
Внутренний диаметр (трубы)	innerDiameter
Материал (трубы)	tubeMaterial
"""

"""
Term (Russian)	Translation	Variable Name
Труба	Tube	tube
Трубопровод	Pipeline	pipeline
Трубопроводная система	Piping system	pipingSystem
Наружный диаметр (трубы)	Outer diameter	outerDiameter
Теплоноситель	Heat carrier	heatCarrier
Масса сухой трубы	Mass of dry tube	dryTubeMass
Масса заполненной трубы	Mass of filled tube	filledTubeMass
Толщина стенки	Wall thickness	wallThickness
Внутренний диаметр (трубы)	Inner diameter	innerDiameter
Материал (трубы)	Material	tubeMaterial
Плотность материала	Density of material	materialDensity
Физические свойства жидкости	Physical properties of the liquid	liquidProperties
"Живое" сечение трубы (по внутреннему диаметру)	"Live" cross-sectional area (by inner diameter)	liveCrossSection
"""


from iapws import IAPWS97
class water(IAPWS97):
	"""
	умеет складывать водЫ
	"""
	def __init__(self, t=20, B=101325, m=1):
		"""Constructor"""
		super().__init__(T=273.16+t, P=B*1e-6)
		self.t = t
		self.B = B
		self.m = m
	@property
	def t(self):
		return self.__t
	@t.setter
	def t(self,t):
		super().__init__(T=273.16+t, P=self.P)
		self.__t = t
	@property
	def B(self):
		return self.__B
	@B.setter
	def B(self,B):
		super().__init__(T=self.T, P=B*1e-6)
		self.__B = B
	def __add__(self,other):
		t = (self.t*self.m + other.t*other.m) / (self.m + other.m)
		return water(t = t)

def din(dout,th):
	return dout-2*th

soft = (
	(4.76, 0.8),
	(6.35, 0.8),
	(7.94, 0.8),
	(9.52, 0.8),
	(12.7, 0.8),
	(15.87, 1.0),
	(19.05, 1.0))
for tube in soft:
	print( "{:4.2f} {:4.2f}".format(tube[0], din(*tube)))

for tube in soft:
	print( "{:4.2f} {:4.2f}".format(din(*tube), tube[0]))

	
straight = (
	(9.52, 0.75),
	(12.79, 0.8),
	(15.87, 0.8),
	(19.05, 0.8),
	(22.22, 1.0),
	(28.57, 1.0),
	(34.92, 1.25),
	(41.27, 1.25),
	(53.97, 1.65),
	(66.67, 2.0),
	(79.37, 2.30),
	(92.08, 2.5),
	(104.77, 2.85))

for tube in straight:
	print( "{:4.2f} {:4.2f}".format(din(*tube), tube[0]))


def M(D, s, rho=7850, l=1):
	"""Масса L м трубы
D - наружный диаметр, м
s - толщина стенки, м
rho - плотность материала, кг/м3
l - длина участка, м (l=1м)"""
	A = lambda d: math.pi * d**2/4
	return rho*(A(D) - A(D-2*s))*l

class Pipe:
    _sizepattern = re.compile('(\d+)[xXхХ](\d[.,]?\d+)')
    def __init__ (self, size, material=None, fluid=None):
        self.size=size
        self._sizes()
        self.fluid=fluid

    @property
    def size(self):
        return self._size
    @size.setter
    def size(self, value):
        if self._sizepattern.match(value):
            self._size=value
    @property
    def OD(self):
        return self._od
    @OD.setter
    def OD(self, value):
        self._od = float(value) * 1e-3
    @property
    def THK(self):
        return self._thk

    @THK.setter
    def THK(self, value):
        self._thk = float(
            re.sub(',' , '.' , value)) *1e-3
    @property
    def ID(self):
        return self.OD - 2*self.THK

    def _sizes(self):
        self.OD, self.THK = self._sizepattern.search(self.size).groups()

    @property
    def csarea(self):
        return math.pi * self.ID**2/4

    @property
    def fcsarea(self):
        return math.pi * self.OD**2/4

    def M(self, rho=7850, L=1):
        """Масса L м трубы
D - наружный диаметр, м
s - толщина стенки, м
rho - плотность материала, кг/м3
L - длина участка, м (l=1м)"""
        return rho*(self.fcsarea-self.csarea)*L

    def MM(self, rho=1.2, L=1):
        """Масса L м трубы теплоносителем
rho - плотность теплоносителя, кг/м3
L - длина участка, м (l=1м)"""

        if self.fluid is not None:
            rho = self.fluid.rho
        return self.csarea*rho*L + self.M()
